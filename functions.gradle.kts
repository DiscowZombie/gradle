ext {
    infix fun String?.ifNullOrBlank(def: () -> String?): String? = if (this.isNullOrBlank()) def.invoke() else this
}